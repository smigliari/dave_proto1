# README #

This repository is for a prototype to serve as a proof of concept for tech choice.

There is not supposed to be any serious functionality in the prototype, just a server, a front-end with some buttons and some visualization, packaging, etc.

This prototype shall use Flask for the server (Python) and React JS client-side.

### Who do I talk to? ###

* Repo owner or admin: Paul Balm - paul@timelabtechnologies.com